Rails.application.routes.draw do
  use_doorkeeper do
    skip_controllers :authorizations, :applications, :authorized_applications
  end
  namespace :api do
    namespace :v1 do
      devise_for :users, controllers: {
        registrations: 'users/registrations',
        sessions: 'users/sessions'
      }

      post "like", to: 'products#asign_like'
      resources :products, only: [:index]
      resources :order, only: [:index, :show, :update, :destroy]
      resources :order_lines, only: [:create]

      post 'password/forgot', to: 'password#forgot'
      post 'password/recover', to: 'password#recover'
      put 'password/update', to: 'password#update_password'
    end
  end

  require 'sidekiq/web'
  mount Sidekiq::Web => "/sidekiq"

  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'

  concern :commentable do
    resources :comments, only: [:create]
  end
  resources :comments, only: [:index, :update, :destroy]
  
  namespace :admin do 
    resources :users
    resources :product, concerns: :commentable
    resources :orders, only: [:index, :show], concerns: :commentable
    get 'product_log', :to => 'productslogs#index'
  end
  
  get "/like", to: 'product#asign_like'
  get "/cart", to: 'cart#index'
  
  resources :orders, only: [:index, :show, :update], concerns: :commentable
  resources :order_lines, only: [:create, :destroy]
  resources :product, except: [:destroy, :create, :new], concerns: :commentable
  resources :profiles, concerns: :commentable
  resources :customers, only: [:index, :new, :create]
  resources :charges
end
