# frozen_string_literal: true

module Stripe
  VERSION = "4.23.0".freeze
end
