# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  def user_email
    UserMailer.user_email(User.all.first, Product.all.first)
  end
  def user_password
    UserMailer.user_password(User.all.first, "4444444444444")
  end
  def password_changed
    UserMailer.password_changed(User.all.first)
  end
end
