require 'rails_helper'

RSpec.describe Product, type: :model do

  it { should have_many(:order_lines) }
  it { should have_many(:orders).through(:order_lines) }
  it { should have_many(:product_prices_log) }
  it { should have_many(:product_tags) }
  it { should have_many(:tags).through(:product_tags) }
  it { should have_many(:likes) }
  it { should have_many(:comments) }

  it { should validate_presence_of(:price) }
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:stock) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:sku) }

  it { should validate_uniqueness_of(:sku) }
  it { should validate_numericality_of(:price) }
  it { should validate_numericality_of(:stock) }

  describe 'update' do
    let!(:product) { create(:product) }
    let!(:admin) { create(:user, admin: 1) }

    context 'when the price is changed' do
      it 'creates an entry in product_prices_log' do
        product.admin_id = admin.id
        product.update(price: 10)
        log = ProductPricesLog.find_by(product_id: product.id)
        expect(log).not_to be_nil
      end
    end

    context 'when other attribute hhas been changed' do
      it 'does not create a log' do
        product.update(stock: 1)
        log = ProductPricesLog.find_by(product_id: product.id)
        expect(log).to be_nil
      end
    end 
  end
end
