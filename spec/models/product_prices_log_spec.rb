require 'rails_helper'

RSpec.describe ProductPricesLog, type: :model do
  it { should belong_to(:product) }
  it { should belong_to(:user) }

  it { should validate_presence_of(:price_before) }
  it { should validate_presence_of(:price_after) }
end
