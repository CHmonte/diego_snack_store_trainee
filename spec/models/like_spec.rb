require 'rails_helper'

RSpec.describe Like, type: :model do
  it { should belong_to(:user) }
  it { should belong_to(:product) }

  it { should validate_presence_of(:kind) }

  describe 'when creating' do
    let!(:user) { create(:user, id: 1) }
    let(:product) { create(:product, like_count: 5, dislike_count: 5) }
    context 'like' do
      let!(:like) { create(:like, user: user, product: product) }
      it 'should increase like count' do
        product.reload
        expect(product.like_count).to eq(6)
      end
    end
    context 'dislike' do
      let!(:like) { create(:like, kind: 'dislike', user: user, product: product) }
      it 'should increase dislike count' do
        product.reload
        expect(product.dislike_count).to eq(6)
      end 
    end
  end
  describe 'when updating' do
    let!(:user) { create(:user, id: 1) }
    let(:product) { create(:product, like_count: 5, dislike_count: 5) }
    context 'with like' do
      let(:like) { create(:like, user: user, product: product, kind: 'dislike') }
      it 'should decrease dislike count' do
        like.update(kind: 'like')
        product.reload
        expect(product.dislike_count).to eq(5)
      end
    end
    context 'with dislike' do
      let(:like) { create(:like, user: user, product: product) }
      it 'should decrease like count' do
        like.update(kind: 'dislike')
        product.reload
        expect(product.like_count).to eq(5)
      end
    end
  end
end
