require 'rails_helper'

RSpec.describe Order, type: :model do
  it { should have_many(:order_lines) }
  it { should have_many(:products).through(:order_lines) }
  it { should belong_to(:user).optional }
  it { have_many(:comments) }

  it { should validate_presence_of(:status) }
  
  #it do 
   # should define_enum_for(:status)
    #  .with_values(complete: "complete", abandoned: "abandoned", canceled: "canceled", open: "open")
    #  .backed_by_column_of_type(:string) 
  #end

  describe 'Update' do
    let!(:user) { create(:user, id: 2) }
    let(:product) { create(:product, stock: 20) }

    context 'when status is changed' do
      context 'to complete' do
        let!(:order) { create(:order, user: user) }
        let!(:order_line) { create(:order_line, quantity: 10, order: order, product: product) }
        it 'should decrease the stock of the products in order' do
          order.update(status: 'complete')
          product.reload
          expect(order_line.product.stock).to eq(10)
        end
      end
      context 'to another option that is not complete' do
        let(:order) { create(:order, user: user) }
        let(:order_line) { create(:order_line, quantity: 10, order: order, product: product) }
        it 'shouldn\'t decrease the stock of the products in order'  do
          order.update(status: 'canceled')
          product.reload
          expect(order_line.product.stock).to eq(20)
        end
      end
    end
  end
end
