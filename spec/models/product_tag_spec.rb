require 'rails_helper'

RSpec.describe ProductTag, type: :model do
  it { should belong_to(:product) }
  it { should belong_to(:tag) }

  #it { should validate_uniqueness_of(:tag_id).scoped_to(:product_id)}
end
