require 'rails_helper'

RSpec.describe User, type: :model do
  
  it { should have_many(:orders) }
  it { should have_one(:profile) }
  it { should have_many(:product_prices_log) }
  it { should have_many(:likes) }
  it { should have_many(:comments) }
end
