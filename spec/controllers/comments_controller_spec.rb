require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  describe 'POST /create' do
    let!(:user) { create(:user) }
    let!(:product) { create(:product) }
    context 'when commenting a' do
      let(:valid_attributes) { { comment: { body: 'hola', user_id: user.id, id: product.id, rating: 5, type: 'Product' }, product_id: product.id } }
      before { sign_in user}
      context 'product' do
        before { post :create, xhr: true, params: valid_attributes }
        it 'should create a comment' do
          expect(product.comments.all.first.body).to eq('hola')
        end
      end
    end
  end
end
