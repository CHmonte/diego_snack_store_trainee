require 'rails_helper'

RSpec.describe Admin::ProductController, type: :controller do
  let(:user) { create(:user, admin: 1) }
  before { sign_in user }
  describe 'GET admin/product' do
    context 'without filters' do
      before { get 'index' }
      it 'it should response with status 200' do
        expect(response).to have_http_status(200)
      end

      it 'should render the show template' do
        expect(response).to render_template(:index)
      end
    end
  end

  describe 'GET admin/product/:id' do
    context 'without filters' do
      let(:product) { create(:product) }
      let(:valid_attributes) { { id: product.id } }
      before { get :show, params:  valid_attributes }
      it 'should response with status 200' do
        expect(response).to have_http_status(200)
      end

      it 'should render the show template' do
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'GET /admin/product/new' do
    before { get :new }
    it 'should response with status 200' do
      expect(response).to have_http_status(200)
    end

    it 'should render the show template' do
      expect(response).to render_template(:new)
    end
  end

  describe 'POST /admin/products' do
    let(:tag) { create(:tag) }
    let(:valid_attributes) { { product: { name: 'Quesitos', sku: '555555', stock: 10, description: ':c', price: 50.00, tags_ids: [tag.id] } } }
    let(:invalid_attributes) { { product: { name: 'Quesitos', sku: '555555', stock: 0, description: ':c', price: 0.00, tags_ids: [tag.id] } } }
    
    context 'when valid product' do
      before { post :create, params: valid_attributes }
      it 'should create a new product' do
        expect(Product.all.first).not_to be_nil
      end
    end

    context 'when invalid product' do
      before { post :create, params: invalid_attributes }
      it 'should not create a new product' do
        expect(Product.all.first).to be_nil
      end
    end
  end

  describe 'PUT /admin/prodcut/:id' do
    let(:tag) { create(:tag) }
    let(:product) { create(:product) }
    let(:valid_attributes) { { product: { name: 'Quesitos', sku: '40000', stock: 50, description: 'churros quesitos', price: 50.00, tags_ids: [tag.id] }, id: product.id } }
    let(:invalid_attributes) { { product: { name: 'Quesitos', sku: '40000', stock: 0, description: 'churros quesitos', price: 0.00, tags_ids: [tag.id] }, id: product.id } }
    
    context 'when valid params' do
      before { post :update, params: valid_attributes }
      it 'should update the product stock' do
        expect(product.reload.stock).to eq(50)
      end
    end

    context 'when invalid params' do
      before { post :update, params: invalid_attributes }
      it 'should update the product stock' do
        expect(product.reload.stock).to eq(20)
      end
    end
  end

  describe 'DELETE /admin/produc/:id' do
    let(:product) { create(:product) }
    before { delete :destroy, params: { id: product.id } }
    it 'should deactive the product' do
      expect(product.reload.active).to be false
    end
  end
end
