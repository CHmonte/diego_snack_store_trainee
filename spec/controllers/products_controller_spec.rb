require 'rails_helper'

RSpec.describe ProductController, type: :controller do
  describe 'GET /product' do
    context 'without filters' do
      before { get :index }
      it 'it should response with status 200' do
        expect(response).to have_http_status(200)
      end

      it 'should render the show template' do
        expect(response).to render_template(:index)
      end
    end
  end

  describe 'GET /product/:id' do
    context 'without filters' do
      let(:product) { create(:product) }
      let(:valid_attributes) { { id: product.id } }
      before { get :show, params:  valid_attributes }
      it 'should response with status 200' do
        expect(response).to have_http_status(200)
      end

      it 'should render the show template' do
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'GET /like' do
    let(:product) { create(:product) }
    let(:valid_attributes) { { type: 'like', product: product.id } }
    before { get :asign_like, xhr: true, params: valid_attributes }
    it 'it should add a like to the product' do
      expect(product.likes).not_to be_nil
    end
  end
end
