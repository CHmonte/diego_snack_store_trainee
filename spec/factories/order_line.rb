FactoryBot.define do 
  factory :order_line do
      order_id {1}
      product_id {1}
      quantity {10}
      price {45.00}
  end
end