FactoryBot.define do 
  factory :order do
    user_id {2}
    status {'open'}
    total {20.00}
    completed_at {nil}
  end
end