FactoryBot.define do 
  factory :product do
    name {'Quesitos'}
    price {50.00}
    stock {20}
    description {'churros quesitos'}
    sku {'40000'}
  end
end