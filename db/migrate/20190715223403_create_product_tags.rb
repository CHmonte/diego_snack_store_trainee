class CreateProductTags < ActiveRecord::Migration[5.2]
  def change
    create_table :product_tags, id: false do |t|
      t.references :tag, foreign_key: true
      t.references :product, foreign_key: true

      t.index [:tag_id, :product_id], unique: true
      t.timestamps
    end
  end
end
