class CreateOrderLines < ActiveRecord::Migration[5.2]
  def change
    create_table :order_lines do |t|
      t.references :order, foreign_key: { on_delete: :cascade }
      t.references :product, foreign_key: { on_delete: :cascade }
      t.decimal :price
      t.integer :quantity
      t.decimal :total, :precision => 10, :scale => 2
      t.timestamps
    end
  end
end
