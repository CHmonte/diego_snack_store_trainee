class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.text :description
      t.string :name
      t.decimal :price, :precision => 10, :scale => 2
      t.string :sku, unique: true
      t.integer :stock
      t.boolean :active, :default => true
      t.integer :like_count, :default => 0
      t.integer :dislike_count, :default => 0

      t.timestamps
    end
  end
end
