class CreateProductPricesLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :product_prices_logs, id: false do |t|
      t.references :product, foreign_key: true
      t.decimal :price_before
      t.decimal :price_after
      t.references :admin, foreign_key: { to_table: :users }

      t.timestamps
    end
  end
end
