class AddEnumToLike < ActiveRecord::Migration[5.2]
   def up
    execute <<-SQL 
      CREATE TYPE like_options AS ENUM ('dislike', 'like');
    SQL

    add_column :likes, :kind, :like_options, null: false
  end

  def down
    remove_column :likes, :kind

    execute <<-SQL
      DROP TYPE like_options;
    SQL
  end
end
