class AddEnumToOrder < ActiveRecord::Migration[5.2]
  def up
    execute <<-SQL 
      CREATE TYPE status_options AS ENUM ('abandoned', 'canceled', 'open', 'complete');
    SQL

    add_column :orders, :status, :status_options, null: false
  end

  def down
    remove_column :orders, :status
    
    execute <<-SQL
      DROP TYPE status_options;
    SQL
  end
end
