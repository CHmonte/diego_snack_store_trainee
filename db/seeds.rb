require 'securerandom'
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Users
u1 = User.create(
      email: "example@example.com",
      password: "1234567"
      )

u1.profile.update(first_name: "Diego", last_name: "Monterrosa", address: "Apopa", phone: "22167897")

u2 = User.create(
      email: "example2@example.com",
      password: "1234567",
      admin: 1
      )
u2.profile.update(first_name: "Jose", last_name: "Azcunaga", address: "Nejapa", phone: "88619752")

u3 = User.create(
      email: "admin@example.com",
      password: "admin01",
      admin: 2
      )

tags = ["shoes","clothes","smartphone","food","tools","computers","car","health","toys","movies"]

10.times do |i|
  Tag.create(
    name: tags[i]
    )  
end

10.times do |i|
  Product.create(
    sku: SecureRandom.uuid,
    price: rand(10.00..100.00),
    stock: rand(1..25),
    name: Faker::Commerce.product_name,
    description: Faker::ChuckNorris.fact
    )
end

20.times do |i|
  ProductTag.create(
    tag_id: Tag.all.sample.id,
    product_id: Product.all.sample.id
    )
end

#Orders
enum_values = Order.statuses.values
customers = User.find(1)
10.times do |i|
  Order.create(
    user_id: customers.id,
    status: 'complete',
    completed_at: Faker::Date.between(2.year.ago, Date.today),
    total: 0
  )
end

#OrderLine
products = Product.all
orders = Order.all
10.times do |i|
  product_sample = products.sample
  OrderLine.create(
    order_id: orders.sample.id,
    product_id: product_sample.id,
    price: product_sample.price,
    quantity: rand(1..product_sample.stock)
  )
end
