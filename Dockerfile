FROM ruby:2.5.3

ARG uid
ARG user

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs postgresql-client

RUN useradd -rm -u $uid -g root $user

RUN mkdir /myapp
RUN chown $user /myapp

USER $user
WORKDIR /myapp

COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
COPY . /myapp
USER root

COPY ./dockerize_sidekiq.sh /
RUN chmod +x /dockerize_sidekiq.sh

COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

RUN mkdir /gembox
RUN chown $user /gembox

USER $user

ENV BUNDLE_PATH=/gembox \
    BUNDLE_BIN=/gembox/bin \
    GEM_HOME=/gembox
ENV PATH="${BUNDLE_BIN}:${PATH}"