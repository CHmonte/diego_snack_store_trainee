class Admin::ProductCreateForm
  include ActiveModel::Model
  
  attr_accessor :name
  attr_accessor :price
  attr_accessor :stock
  attr_accessor :description
  attr_accessor :tag_ids
  attr_accessor :sku
  attr_accessor :picture

  validates :name, :price, :stock, :sku, :description, presence: true

  def save
    return false unless valid?
    product = Product.new(name: name, price: price, stock: stock, description: description, sku: sku, picture: picture)
    if product.save
      if tag_ids.present?
        tag_ids&.each do |tag|
          product.product_tags.create!(tag_id: tag)
        end
      else
        true
      end
    else
      false
    end
  end
end