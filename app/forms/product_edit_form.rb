class ProductEditForm
  include ActiveModel::Model
  
  attr_accessor :name
  attr_accessor :price
  attr_accessor :stock
  attr_accessor :description
  attr_accessor :tag_ids
  attr_accessor :id
  attr_accessor :sku
  attr_accessor :admin
  attr_accessor :picture

  validates :id, :name, :price, :stock, :admin, :sku, :description, presence: true

  def save
    return false unless valid?

    product = Product.find(id)
    price_before = product.price
    product.picture.purge_later
    product.picture.attach(picture)
    if product.update(name: name, price: price, stock: stock, description: description)
      product.product_tags.delete_all
      tag_ids&.each do |tag|
        product.product_tags.create!(tag_id: tag)
      end
      create_log_for_price(price_before)
    else
      false
    end
  end

  private 

  def create_log_for_price(price_before)
    return true if price == price_before

    ProductPricesLog.new(product_id: id,
          admin_id: admin,
          price_before: price_before,
          price_after: price).save
  end
end
