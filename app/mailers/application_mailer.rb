class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@dsnackstore.com'
  layout 'mailer'
end
