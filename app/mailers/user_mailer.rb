class UserMailer < ApplicationMailer

  def user_email(user, product)
    @user = user
    @product = product
    mail(to: @user.email, subject: 'Product running out')
  end

  def user_password(user, token)
    @user = user
    @token = token
    mail(to: @user.email, subject: 'Password recovery')
  end

  def password_changed(user)
    @user = user
    mail(to: @user.email, subject: 'Email succesfully changed')
  end
end
