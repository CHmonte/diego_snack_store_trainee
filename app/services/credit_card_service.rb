class CreditCardService
  def initialize(user_id, token)
    @user = User.find(user_id)
    @token = token
  end
 
  def create_credit_card
    if @token
      customer = Stripe::Customer.retrieve(@user.stripe_id)
      customer.sources.create(source: @token).id
    else
      flash[:error] = ["An error has ocurred, confirmation token is missing"]
      redirect_to cart_path 
    end
  end
end