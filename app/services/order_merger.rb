class OrderMerger
  def initialize(check, user, session, signed_in)
    @checked = check
    @user = user
    @order_in_session = session
    @signed_in = signed_in
  end

  def call
    if @signed_in
      check_user
    else
      asign_order_for_guest
    end
  end

  private

  def asign_order_for_guest
    if @order_in_session
      Order.where(status: 'open').find(@order_in_session)
    else  
      Order.create(total: 0, status: 'open')
    end
  end

  def check_user
    unless @checked
      check_order_in_session
    else
      Order.find(@order_in_session)
    end    
  end

  def check_order_in_session
    order = @user.orders.where(status: 'open').first
    if @order_in_session
      checl_open_order_for_session(order)
    else
      check_open_order(order)
    end
  end

  def check_open_order_for_session(order)
    if order
      mergeorder(order)
    else
      Order.where(status: 'open').find(@order_in_session).update!(user_id: current_user.id)
    end
  end

  def check_open_order(order)
    if order
      order
    else
      @user.orders.create!(total: 0, status: 'open')
    end
  end

  # this method compare all the order lines stored in the order stored in session with all the order lines in the last open order
  # saved in database, this will make a copy of each record in the last open order and will change the status of order in session to abandoned  
  def mergeorder(user_order)
    order_session = Order.find(@order_in_session)
    order_session.order_lines.each do |sorln|
      product_match= false
      user_order.order_lines.each do |uorln|
        if uorln.product_id == sorln.product_id
          sum = uorln.quantity + sorln.quantity
          uorln.update!(quantity: sum)
          product_match = true
        end
      end
      unless product_match 
        new_record = sorln.dup
        new_record.update!(order_id: user_order.id)
      end
    end
    order_session.update(status: 'abandoned')
    user_order
  end
end
