class ProductFilter
  def initialize(params)
    # @filter has the complete value of params
    @filter = params
  end

  def call
    return @filter unless @filter[:filter].present?
    
    using_historic @filter[:filter]
  end

  private
  def using_historic(filter)
    if filter[:historic].present?
      updating_historic(filter)
    else
      create_historic(filter)
    end
  end

  def updating_historic(filter)
    searcher = filter[:historic]
    searcher[:tags] = updating_tags(searcher, filter)
    searcher[:search] = updating_search(searcher, filter)
    searcher[:scope] = updating_scope(searcher, filter)
    searcher = { historic: searcher.to_unsafe_h }
  end

  def updating_tags(searcher, filter)
    return searcher[:tags] unless filter[:tag].present?
    
    return get_tag(filter) unless searcher[:tags].present?
    
    add_or_take_tag(searcher[:tags], filter)
  end

  def add_or_take_tag(tags, filter)
    new_tag = get_tag(filter).first
    if tags.include?(new_tag)
      tags.delete(new_tag)

      return nil if tags.empty?
      tags
    else
      tags << new_tag
    end
  end

  def updating_search(searcher, filter)
    return searcher[:search] unless filter[:search].present?
    
    get_search(filter) 
  end

  def updating_scope(searcher, filter)
    return searcher[:scope] unless filter[:scope].present?
    
    get_scope(filter) 
  end

  def create_historic(filter)
    searcher = { historic: { 
                      search: get_search(filter),
                      tags: get_tag(filter),
                      scope: get_scope(filter)
                      } }
    searcher
  end

  def get_tag(filter)
    [filter[:tag]] if filter[:tag].present?
  end

  def get_scope(filter)
    filter[:scope] if filter[:scope].present?
  end

  def get_search(filter)
    filter[:search] if filter[:search].present?
  end
end
