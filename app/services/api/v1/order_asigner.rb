class Api::V1::OrderAsigner
  def initialize(user, order = nil)
    @user = user
    @order = order
  end

  def call
    order_in_db = Order.where(status: 'open').find_by(user_id: @user.id)
    unless order_in_db.present?
      order_in_db = Order.create(user_id: @user.id, total: 0, status: 'open')
    end
    return order_in_db unless @order
    merge_order(order_in_db, @order)
  end

  private

  def merge_order(orderdb, order)
    order["products"].each do |sorln|
      product_match= false
      orderdb.order_lines.each do |uorln|
        if uorln.product_id == sorln["id"]
          sum = uorln.quantity + sorln["quantity"]
          uorln.update!(quantity: sum)
          product_match = true
        end
      end
      unless product_match 
        product = Product.find(sorln["id"])
        new_order_line = OrderLine.create(order_id: orderdb.id, product_id: product.id, price: product.price, quantity: sorln["quantity"], total: 0)
      end
    end
    orderdb.reload
  end
end