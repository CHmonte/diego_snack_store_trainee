class UserQuery
  attr_reader :relation

  def initialize(relation = User.all)
    @relation = relation 
  end

  def get_users
    relation.where('admin < 2')
  end

  def user_by_email(email)
    relation.where("email LIKE ?", "%#{email}%")
  end

  def get(id)
    relation.find(id)
  end

  def get_users_active
    get_users.where('active = true')
  end
end
