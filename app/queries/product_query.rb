class ProductQuery
  attr_reader :relation

  def initialize(relation = Product.all)
    @relation = relation 
  end

  def active
    relation.where('active = true')
  end

  def get(id)
    relation.find(id)
  end

  def get_products_by_tags(tags)
    relation.joins(:tags)
      .where('tags.id in (?)', tags)
      .having('COUNT(1) >= ? ', tags.size)
      .group('products.id')
  end

  def with_name_like(name)
    relation.where('products.name LIKE ?', "%#{name}%")
  end

  def apply_scope(scope)
    case scope
    when 'likes'
      relation.sclikes
    when 'dislikes'
      relation.scdislikes
    when 'names'
      relation.scnames
    else
      relation
    end
  end
end
