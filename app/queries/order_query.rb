class OrderQuery
  attr_reader :relation

  def initialize(relation = Order.all)
    @relation = relation 
  end

  def order_from_user(id)
    relation.where("user_id = ?", id)
  end

  def get(id)
    relation.find(id)  
  end
end