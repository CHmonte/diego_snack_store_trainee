class Admin::ProductslogsController < ApplicationController

  def index
    @logs = ProductPricesLog.all
    authorize [:admin, @logs]
  end
end
