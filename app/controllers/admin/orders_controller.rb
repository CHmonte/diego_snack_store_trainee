class Admin::OrdersController < ApplicationController
  def index
    @orders = Order.all
    authorize [:admin, Order]
  end

  def show
    authorize [:admin, Order]
    @order = OrderQuery.new.get(params[:id])
    setup_next_comment('Order', @order)
  end
end
