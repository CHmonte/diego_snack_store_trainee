class Admin::UsersController < ApplicationController
  before_action :set_user, except: [:index, :new, :create]

  def index
    authorize [:admin, User]
    @users = UserQuery.new.get_users     
    if params[:filter] && params[:filter][:search].present?
      @users = UserQuery.new.user_by_email(params[:filter][:search])
    end
    @users = @users.paginate(page: params[:page], per_page: 6)
  end

  def new
    authorize [:admin, User]
    @user = User.new
  end

  def create
    authorize [:admin, User]
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "The user has been created successfully"
      redirect_to admin_users_path
    else
      flash[:error] = @user.errors.messages
      redirect_to new_admin_user_path
    end 
  end
  
  def update
    authorize [:admin, User]
    if @user.admin == 1
      @user.admin = 0
    elsif @user.admin == 0
      @user.admin = 1
    end
    if  @user.save
      flash[:success] = "The role has been updated"
    else
      flash[:error] = ["An error has ocurred the user couldn't be updated"]
    end
    redirect_to admin_users_path
  end

   def destroy
    authorize [:admin, User]
    if  @user.update(active: !@user.active)
      flash[:success] = "The User estated has been changed successfully"
    else
      flash[:error] = ["An error has ocurred the user couldn't be deactivated"]
    end
    redirect_to admin_users_path
  end

  private
  def set_user
    @user = UserQuery.new.get(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :admin)
  end
end
