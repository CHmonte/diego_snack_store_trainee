# frozen_string_literal: true
require 'product_edit_form.rb'
require 'admin/product_new_form.rb'
require 'products_filter.rb'
require 'securerandom'

class Admin::ProductController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy] 
  
  def index
    authorize [:admin, Product]
    @product = Product.all
    @tags = Tag.all
    @searcher = ProductFilter.new(params).call
    # if a filter has been used @searcher now contains a hash with a key call historic inside this key
    # there will be three more keys: tags, scope and search each one stores the value from the last filter applied 
    if @searcher[:historic]
      @product = filter_by_tags(@product, @searcher[:historic])
      @product = ProductQuery.new(@product).apply_scope(@searcher[:historic][:scope])
      @product = ProductQuery.new(@product).with_name_like(@searcher[:historic][:search])
    end
    @product = @product.paginate(page: params[:page], per_page: 6)
  end

  def show
    authorize [:admin, @product]
    @p = @product
    # it setups the variables needed to make a new comment
    setup_next_comment('Product', @product)
  end

  def new
    authorize [:admin, Product]
    @product = Product.new
    @tags = Tag.all
  end

  def create
    authorize [:admin, Product]
    @form = Admin::ProductCreateForm.new(product_params)
    @form.assign_attributes(sku: SecureRandom.uuid)
    if @form.save
      flash[:success] = "The product has been created successfully"
      redirect_to admin_product_index_path
    else
      flash[:error] = [@form.errors.messages]
      redirect_to new_admin_product_path
    end
  end
  
  def edit
    authorize [:admin, @product]
    @tags = Tag.all
  end
  
  def update
    authorize [:admin, @product]
    @form = ProductEditForm.new(product_params)
    @form.assign_attributes(id: @product.id, sku: @product.sku, admin: current_user.id)
    if @form.save
      flash[:success] = "Product updated"
      redirect_to admin_product_index_path
    else
      flash[:error] = ["There has been a problem updating the product. #{form.errors.messages}"]
      redirect_to edit_admin_product_path(product_params)
    end 
  end

  def destroy
    authorize [:admin, @product]
    if @product.update(active: false)
      flash[:success] = "The product has been deactivated"
    else
      flash[:error] = ["An error has ocurred the product couldn't be deactivated"]
    end
    redirect_to admin_product_index_path
  end

  private
  
  def set_product
    @product = ProductQuery.new.get(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :price, :stock, :description, :picture, :tag_ids => [])
  end

  def filter_by_tags(products, historic)
    return products unless historic[:tags].present?
    
    products = ProductQuery.new(products).get_products_by_tags(historic[:tags])
  end

  def user_like(product)
    # search for the user's like, this is need because there is only like/dislike of user for each product
    Like.where('user_id = ? AND product_id = ?', current_user.id, product).first
  end
end
