class CartController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  
  def index
    authorize Order
    @order = OrderMerger.new(session[:checked], current_user, session[:order_id], user_signed_in?).call
    session[:checked] = true
    session[:order_id] = @order.id
    # it setups the variables for a new comment
    setup_next_comment("Order", @order)
  end
end
