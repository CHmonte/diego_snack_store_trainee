class ChargesController < ApplicationController
  before_action :authenticate_user!
  before_action :get_order, except: [:new]
  
  def new
    respond_to do |format|
      format.js
    end
  end

  def create
    stripe_card_id = CreditCardService.new(current_user.id, params[:stripeToken]).create_credit_card
    unless @order.total > 0
      flash[:error] = ['Order\'s total can\'t be 0']
      redirect_to cart_path
      return
    end
    Stripe::Charge.create(
      customer: current_user.stripe_id,
      source:   stripe_card_id,
      amount:   @order.total_to_cents.to_i,
      currency: 'usd'
    )
  
    if @order.update(status: 'complete', completed_at: Time.now.getutc)
      session['checked'] = nil
      session[:order_id] = nil
      flash[:success] = "Checkout completed with success"
      redirect_to root_path
    else
      flash[:error] = ["An has ocurred with the database, your order couldn't be processed"]
      redirect_to cart_path
    end

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to cart_path
  end

  private
  
  def get_order
    @order = Order.find(params[:order_id])
  rescue ActiveRecord::RecordNotFound => e
    flash[:error] = ['Order not found!']
    redirect_to root_path
  end
end
