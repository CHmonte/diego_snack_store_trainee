# frozen_string_literal: true

#  Home page controller
class HomeController < ApplicationController
  before_action :authenticate_user!, except: [:index]

  def index; end
end
