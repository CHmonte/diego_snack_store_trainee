class OrdersController < ApplicationController
  before_action :set_order, only: [ :update ]
  before_action :authenticate_user!

  def index
    @orders = OrderQuery.new.order_from_user(current_user.id)
  end

  def show
    @order = OrderQuery.new.get(params[:id])
    setup_next_comment('Order', @order)
  end

  private

  def set_order
    @order = OrderQuery.new.get(session[:order_id])
  end
end
