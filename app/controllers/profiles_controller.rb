class ProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_profile
  def show
    @p = @profile
    # it setups the variables needed to make a new comment
    setup_next_comment('Profile', @profile)
  end
  
  def edit
    authorize @profile
  end

  def update
    authorize @profile
    if @profile.update(profile_params)
      flash[:success] = "Profile updated"
      redirect_to profile_path(params[:id])
    else
      flash[:error] = ["There has been a problem updating your profile, please check all the fields have valid values"]
      redirect_to edit_admin_product_path(profile_params)
    end
  end

  private

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :phone, :address)
  end

  def set_profile
    @profile = Profile.find(params[:id])
  end
end
