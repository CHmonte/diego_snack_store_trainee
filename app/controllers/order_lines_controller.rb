class OrderLinesController < ApplicationController
  before_action :authenticate_user!, except: [:create, :destroy]
  before_action :current_order

  def create
    search_order = OrderLine.where(product_id: order_line_params[:product_id]).find_by(order_id: session[:order_id])
      if search_order
        sum = search_order.quantity + order_line_params[:quantity].to_i
        if search_order.update(quantity: sum)
          flash[:success] = "The product in your cart has been updated"
        else
          flash[:error] = ["An error has ocurred your product couldn't be updated"]
        end
      else
        @order_line = OrderLine.new(order_line_params)
        @order_line.order_id = session[:order_id]
        @order_line.price = Product.find(order_line_params[:product_id]).price
        if @order_line.save
          flash[:success] = "The product has been added to your cart"
        else
          flash[:error] = ["An error has ocurred your product couldn't be updated, #{@order_line.errors.messages}"]
        end
      end
    redirect_to cart_path 
  end

  def destroy
    @order_line = OrderLine.find(delete_param[:id])
    if @order_line.destroy
      flash[:success] = "The product has been eliminated from your cart"
    else
      flash[:error] = ["An error has ocurred your product couldn't be updated"]
    end
    redirect_to cart_path
  end

  private

  def order_line_params
    params.require(:order_line).permit(:product_id, :price, :quantity)
  end

  def delete_param
    params.permit(:id)
  end

  def current_order
    @order = OrderMerger.new(session[:checked], current_user, session[:order_id], user_signed_in?).call
    session[:checked] = true
    session[:order_id] = @order.id
  end
end
