class CustomersController < ApplicationController
  before_action :authenticate_user!, except: %i[index]
  def index
    authorize User
    @users = UserQuery.new.get_users_active
    if params[:filter] && params[:filter][:search].present?
      @users = UserQuery.new.user_by_email(params[:filter][:search])
    end
    @users = @users.paginate(page: params[:page], per_page: 6)
  end

  def new
    authorize User
    @user = User.new 
  end

  def create
    authorize User
    @user = User.new(user_params)
    if @user.save
       flash[:success] = "The user has been created successfully"
      redirect_to customers_path
    else
      flash[:error] = [@user.errors.messages]
      redirect_to new_customer_path
    end
  end
  
  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation) 
  end
end
