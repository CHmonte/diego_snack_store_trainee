class CommentsController < ApplicationController
  
  def index
    #comments in the list for admin
    authorize Comment
    @comments = Comment.all.where('publish = false')
  end

  def create
    # it obtains the correct to which the comment is been created
    param_id = params[:comment][:type].downcase + "_id"
    object = params[:comment][:type].safe_constantize
    object = object.find(params[param_id])
    comment = object.comments.new(comment_params)
    comment.user = current_user
    comment.publish = false if object.is_a?(Profile)
    object.save
    
    # it setups the variables for the next comment
    setup_next_comment(params[:comment][:type], object)
  end

  def update
    authorize Comment
    @comment = Comment.find(params[:id])
    if @comment.update(publish: true)
      flash[:success] = "Review published"
      redirect_to comments_path
    else
      flash[:error] = ["#{@form.errors.messages}"]
      redirect_to comments_path
    end
  end

  def destroy
    @comment = Comment.find(params[:id])
    authorize @comment
    if @comment.destroy
      flash[:success] = "Review rejected"
      redirect_to comments_path
    else
      flash[:error] = ["#{@form.errors.messages}"]
      redirect_to comments_path
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :rating)
  end
end
