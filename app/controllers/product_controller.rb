# frozen_string_literal: true
require 'product_edit_form'
require 'products_filter.rb'

# product controller
class ProductController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  before_action :set_product, only: [:show,:edit, :update] 

  def index
    @product = ProductQuery.new.active
    @tags = Tag.all
    @searcher = ProductFilter.new(params).call
    # if a filter has been used @searcher now contains a hash with a key call historic inside this key
    # there will be three more keys: tags, scope and search each one stores the value from the last filter applied 
    if @searcher[:historic]
      @product = filter_by_tags(@product, @searcher[:historic])
      @product = ProductQuery.new(@product).apply_scope(@searcher[:historic][:scope])
      @product = ProductQuery.new(@product).with_name_like(@searcher[:historic][:search])
    end
    @product = @product.paginate(page: params[:page], per_page: 6)
  end

  def show
    @p = @product
    # it setups the variables needed to make a new comment
    setup_next_comment('Product', @product)
  end

  def asign_like
    return unless current_user
    
    record_like = Like.user_like(current_user.id, params[:product]).first
    # a user can only give one kind of like to each product 
    # if the other kind of like is selected the former will be removed
    if record_like.present?
      if record_like.kind == 'like' && params[:type] == 'dislike'
        record_like.update!(kind: 'dislike')
      elsif record_like.kind == 'dislike' && params[:type] == 'like'
        record_like.update!(kind: 'like')
      end
    else
      product = ProductQuery.new.get(params[:product])
      product.likes.create!(user_id: current_user.id, kind: params[:type])
    end
    @p = ProductQuery.new.get(params[:product])
  end

  def edit
    authorize @product
    @tags = Tag.all
  end
  
  def update
    authorize @product
    @form = ProductEditForm.new(product_params)
    @form.assign_attributes(id: @product.id, price: @product.price, sku: @product.sku, admin: current_user.id)
    if @form.save
      flash[:success] = "Product updated"
      redirect_to product_index_path
    else
      flash[:error] = ["There has been a problem updating the product. #{@form.errors.messages}"]
      redirect_to edit_product_path(product_params)
    end
  end

  private

  def set_product
    @product = ProductQuery.new.get(params[:id])
  end
  
  def product_params
    params.require(:product).permit(:name, :stock, :description, :picture, :tag_ids => [])
  end

  def filter_by_tags(products, historic)
    return products unless historic[:tags].present?
    
    products = ProductQuery.new(products).get_products_by_tags(historic[:tags])
  end
end
