# frozen_string_literal: true
require 'pundit'
require 'comment'

# Class Application Controller
class ApplicationController < ActionController::Base
  include Pundit
  include CommentModule

  before_action :authenticate_user!
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

  def user_not_authorized
    flash[:error] = ["You are not authorized to perform this action."]
    redirect_to root_path
  end

  def record_not_found
    redirect_back(fallback_location: root_path, error: "The resource you want to access is not at your reach")
  end
end
