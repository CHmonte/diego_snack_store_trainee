# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]
  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    super
    unless current_user.admin < 2
      @order = OrderMerger.new(session[:checked], current_user, session[:order_id], user_signed_in?).call
      session[:checked] = true
      session[:order_id] = @order.id
    end
  end

  # DELETE /resource/sign_out
  def destroy
    super
    session[:order_id] = nil
    session['checked'] = nil
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
