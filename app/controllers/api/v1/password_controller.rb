module Api
  module V1
    class PasswordController < ApplicationController
      before_action :doorkeeper_authorize!, except: [:forgot, :recover]

      def forgot
        unless params[:email].present?
          return render json: { error: 'Email can be empty'}
        end

        user = User.find_by(email: params[:email])

        if user
          raw, token = Devise.token_generator.generate(User, :reset_password_token)
          user.reset_password_token = token
          user.reset_password_sent_at = Time.now.utc
          user.save!
          MailerFromRecoverPassword.perform_async(raw, user.id)
          head :no_content
        else
          render json: {error: ['Email not found']}, status: :not_found
        end
      end

      def recover
        unless params[:token].present?
          return render json: {error: 'Token empty'}
        end

        user = User.with_reset_password_token(params[:token])

        if user
          byebug
          if user.reset_password_period_valid?
            if user.reset_password(password_params[:password], password_params[:password_confirmation])
              MailerFromPasswordChanged.perform_async(user.id)
              render json: {message: 'Password updated'}, status: :ok
            else
              render json: {errors: user.errors.messages}, status: :unprocessable_entity
            end
          else
            render json: {error: 'Token expired'}, status: :unprocessable_entity
          end
        else
          render json: {error: 'User not found'}, status: :not_found
        end 
      end

      def update_password
        unless params[:password] && params[:password_confirmation]
          render json: {error: 'Password or confirmation not present '}, status: :unprocessable_entity
          return
        end

        if current_user.reset_password(password_update_params[:password],password_update_params[:password_confirmation])
          MailerFromPasswordChanged.perform_async(current_user.id)
          head :no_content
        else
          render json: {errors: current_user.errors.full_messages}, status: :unprocessable_entity
        end
      end

      private

      def password_params
        params.permit(:token, :password, :password_confirmation)
      end


      def password_update_params
        params.permit(:token, :password, :password_confirmation)
      end
    end
  end
end