module Api
  module V1
    class OrderLinesController < ApplicationController
      def create
        @order = OrderAsigner.new(current_user, params["cart"]).call
        order_line = @order.order_lines.find_by(product_id: params["orderline"]["product_id"])
        if order_line.present?
          sum = params["orderline"]["quantity"].to_i + order_line.quantity
          order_line.update!(quantity: sum)
        else
          orderln = OrderLine.new(order_line_params)
          orderln.order_id = @order.id
          orderln.price = Product.find(order_line_params[:product_id]).price
          orderln.save!
        end
        head :created
      end

      private

      def order_line_params 
        params.require("orderline").permit("product_id", "quantity")
      end
    end
  end
end