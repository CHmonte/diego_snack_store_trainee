module Api
  module V1
    class ApplicationController < ActionController::API
      attr_reader :current_user      
      rescue_from StandardError, with: :errors
      before_action :doorkeeper_authorize!
      before_action :current_resource_owner

      respond_to :json

      private

      def errors(ex)
        render :json => { :error => "#{ex.message}"}, status: :bad_request
      end

      def current_resource_owner
        @current_user = User.find(doorkeeper_token.resource_owner_id) if doorkeeper_token
      end
    end
  end
end