module Api
  module V1
    class OrderController < ApplicationController
    
      def index
        @orders = Order.where(user_id: current_user.id)  
      end
    
      def show
        @order = OrderAsigner.new(current_user, params["order"]).call
      end

      def update
        @order = Order.find(params[:order_id])
        stripe_card_id = CreditCardService.new(current_user.id, params[:stripeToken]).create_credit_card

        unless @order.total > 0
          render :json => {error: 'Order\'s total can\'t be 0'}, status: :bad_request
          return
        end

        Stripe::Charge.create(
          customer: current_user.stripe_id,
          source:   stripe_card_id,
          amount:   @order.total_to_cents.to_i,
          currency: 'usd'
        )
  
        @order.update!(status: 'complete', completed_at: Time.now.getutc)
        head :no_content

      rescue Stripe::CardError => e
        render :json => { :error => "#{e.message}"}
      end  

      def destroy
        @order = OrderAsigner.new(current_user).call
        @order.update!(status: 'canceled')
        head :no_content
      end
    end
  end
end