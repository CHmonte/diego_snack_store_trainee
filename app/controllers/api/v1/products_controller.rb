module Api
  module V1
    class ProductsController < ApplicationController
      before_action :doorkeeper_authorize!, only: [:asign_like]
      def index
        @products = Product.all
        if params[:filter].present?
          @products = filter_by_tags(@products, params[:filter])
          @products = ProductQuery.new(@products).apply_scope(params[:filter][:scope])
          @products = ProductQuery.new(@products).with_name_like(params[:filter][:search])
        end
        @products = @products.paginate(page: params[:page], per_page: 6)
      end

      def asign_like
        record_like = Like.user_like(current_user.id, params[:product]).first
        # a user can only give one kind of like to each product 
        # if the other kind of like is selected the former will be removed
        if record_like.present?
          if record_like.kind == 'like' && params[:type] == 'dislike'
            record_like.update!(kind: 'dislike')
          elsif record_like.kind == 'dislike' && params[:type] == 'like'
            record_like.update!(kind: 'like')
          end
        else
          product = ProductQuery.new.get(params[:product])
          product.likes.create!(user_id: current_user.id, kind: params[:type])
        end
        head :no_content
      end

      private

      def filter_by_tags(products, historic)
        return products unless historic[:tags].present?
    
        products = ProductQuery.new(products).get_products_by_tags(historic[:tags])
      end
    end
  end
end