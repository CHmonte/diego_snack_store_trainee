class ProductTag < ApplicationRecord
  belongs_to :tag
  belongs_to :product

  validates :tag_id, uniqueness: { scope: :product_id, case_sensitive: true}
end
