class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :commentable, polymorphic: true

  delegate :email, to: :user, prefix: true
  delegate :user, to: :commentable, prefix: true 
end
