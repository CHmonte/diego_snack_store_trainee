# frozen_string_literal: true

# Module of table products
class Product < ApplicationRecord
  has_many :order_lines
  has_many :orders, through: :order_lines
  has_many :product_prices_log
  has_many :product_tags
  has_many :tags, through: :product_tags
  has_many :likes
  has_many :comments, as: :commentable
  has_one_attached :picture

  validates :price, :stock, :description, :name, :sku, presence: true
  validates :sku, uniqueness: true
  validates :price, numericality: { greater_than_or_equal_to: 10 }
  validates :stock, numericality: { greater_than_or_equal_to: 1 }, on: :create
  validates :stock, numericality: { greater_than_or_equal_to: 0 }, on: :update

  scope :sclikes, -> { order(like_count: :DESC) }
  scope :scdislikes, -> { order(dislike_count: :DESC) }
  scope :scnames, -> { order(:name) }

  after_update :monitor_stock

  private

  def monitor_stock
    return unless stock < 4

    return unless previous_changes['stock'].present?

    MailerFromMonitorStock.perform_async(id) if like_count > 0
  end
end
