class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :orders
  has_one :profile
  has_many :product_prices_log, as: :admin
  has_many :likes
  has_many :comments

  after_create :asign_profile
  before_create :assign_customer_id

  def asign_profile
    create_profile    
  end

  def assign_customer_id
    customer = Stripe::Customer.create(email: email)
    self.stripe_id = customer.id
  end
end
