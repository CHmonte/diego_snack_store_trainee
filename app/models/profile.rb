class Profile < ApplicationRecord
  belongs_to :user
  has_many :comments, as: :commentable
  
  def is_able_to_be_commented?(current_user)
    user != current_user
  end
end
