class OrderLine < ApplicationRecord
  belongs_to :order
  belongs_to :product

  delegate :name, to: :product, prefix: true

  validates :quantity, :price, :order_id, :product_id, presence: true
  validates :quantity, numericality: { greater_than: 0 }
  validates :price, numericality: { greater_than: 0 }

  validate :check_stock
  before_save :calc_total

  private

  def check_stock
    product = Product.find(product_id)

    if product.stock < quantity
      errors.add(:quantity, "Not enough product in stock")
    end
  end

  def calc_total
    self.total = price*quantity
  end
end
