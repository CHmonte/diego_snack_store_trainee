class Order < ApplicationRecord
  belongs_to :user, optional: true
  has_many :order_lines
  has_many :products, through: :order_lines
  has_many :comments, as: :commentable

  enum status: { abandoned: "abandoned", canceled: "canceled", open: "open", complete: "complete" }

  validates :status, presence: true

  before_update :update_stock
  before_update :calc_total

  def total

    order_lines.inject(0) { |sum, orln| sum += orln.total}
  end

  def total_to_cents
    total * 100
  end

  private

  def update_stock
    # It decreases the stock of all products in the order which has been completed
    return unless status == 'complete'
    
    order_lines.each do |orln|
      stock = orln.product.stock - orln.quantity
      orln.product.update!(stock: stock)
    end
  end

  def calc_total
    self.total = order_lines.inject(0) { |sum, orln| sum += orln.total}
  end
end
