class ProductPricesLog < ApplicationRecord
  belongs_to :product
  belongs_to :user, foreign_key: :admin_id

  validates :price_before, :price_after, presence: true
end
