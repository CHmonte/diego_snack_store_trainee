class Like < ApplicationRecord
  belongs_to :user
  belongs_to :product

  enum kind: { dislike: "dislike", like: "like" }

  validates :kind, presence: true

  scope :user_like, ->(user, product) { where('user_id = ? AND product_id = ?', user, product) }

  before_update :take_like
  after_save :calc_like

  private

  # when updating it removes the corresponding kind of like
  def take_like
    if kind_was == "like"
      product.like_count -= 1
    else
      product.dislike_count -= 1
    end
  end

  # when creating or saving a like it updates the counter in the appropriate product
  def calc_like
    if kind == "like"
      product.like_count += 1
    else
      product.dislike_count += 1
    end
    product.save!
  end
end
