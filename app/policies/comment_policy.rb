class CommentPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def index?
    user.admin > 0
  end

  def update?
    user.admin > 0
  end

  def destroy?
    if record.commentable.is_a?(Profile)
      user.admin > 1
    else
      false
    end
  end
end
