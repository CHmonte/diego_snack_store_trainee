class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def index?
    user
  end
  def new?
    user.admin == 1
  end
  def create?
    user.admin == 1
  end
end
