class OrderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def index?
    if user
      user.admin < 1
    else
      true
    end
  end
end
