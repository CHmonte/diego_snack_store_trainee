class ProductPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def edit?
    user.admin == 1
  end
  def update?
    user.admin == 1
  end
end
