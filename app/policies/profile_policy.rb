class ProfilePolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def update?
    user.profile == record  
  end
  def edit?
    user.profile == record
  end
end
