class Admin::OrderPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def index?
    user.admin == 2
  end
  def show?
    user.admin == 2
  end
end
