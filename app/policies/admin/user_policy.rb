class Admin::UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end
  def index?
    user.admin == 2
  end
  def new?
    user.admin == 2
  end
  def create?
    user.admin == 2
  end
  def update?
    user.admin == 2
  end
  def destroy?
    user.admin == 2
  end
end
