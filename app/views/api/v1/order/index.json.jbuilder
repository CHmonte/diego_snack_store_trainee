json.orders @orders.each do |order|
  json.id order.id
  json.status order.status
  json.products order.order_lines.each do |ol|
    json.name ol.product.name
    json.price number_to_currency(ol.price)
    json.quantity ol.quantity
  end
end