json.order do
  json.id @order.id
  json.status @order.status
  json.items @order.order_lines.each do |ol|
    json.name ol.product.name
    json.price number_to_currency(ol.price)
    json.quantity ol.quantity
  end
  json.comments @order.comments.each do |com|
    json.user com.user
    json.text com.body
    json.rate com.rating
  end
end