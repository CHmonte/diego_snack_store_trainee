json.products @products do |product|
  json.id product.id
  json.name product.name
  json.image product.picture.attached? ? product.picture.variant(resize: "220x220!").service_url : "https://wolper.com.au/wp-content/uploads/2017/10/image-placeholder.jpg"
  json.stock product.stock
  json.price number_to_currency(product.price)
  json.like_count product.like_count
  json.dislike_count product.dislike_count
  json.tags product.tags do |t|
    json.id t.id
    json.name t.name
  end
end

json.total_pages @products.total_pages