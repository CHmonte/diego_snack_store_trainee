class MailerFromPasswordChanged
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(user_id)
    user = User.find(user_id)
    UserMailer.password_changed(user).deliver_now
  end
end