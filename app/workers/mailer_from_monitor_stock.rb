class MailerFromMonitorStock
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(product_id)
    like_of_last_user = Like.where(product_id: product_id).where(kind: 'like').first
    return unless like_of_last_user.present?

    product = Product.find(product_id)
    user = User.find(like_of_last_user.user_id)
    UserMailer.user_email(user, product).deliver_now
  end
end
