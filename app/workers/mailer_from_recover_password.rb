class MailerFromRecoverPassword
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(token, user_id)
    user = User.find(user_id)
    UserMailer.user_password(user, token).deliver_now
  end
end