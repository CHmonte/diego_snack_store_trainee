{
  "swagger": "2.0",
  "info": {
    "description": "This an online snack store, where you are able to buy, like, review and rate snacks from all over the world",
    "version": "0.7.0",
    "title": "Diego Snack Store",
    "termsOfService": "",
    "contact": {
      "email": "joseazcunaga4@gmail.com"
    },
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "host": "localhost:3000",
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "password",
      "description": "Manage your passwords"
    },
    {
      "name": "product",
      "description": "Products of the snack store"
    },
    {
      "name": "order",
      "description": "User's orders"
    },
    {
      "name": "order_line",
      "description": "Products in each order"
    },
    {
      "name": "login",
      "description": "Login"
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/password/update": {
      "put": {
        "tags": [
          "password"
        ],
        "summary": "Update user password",
        "description": "",
        "operationId": "updatePass",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "password",
            "type": "string",
            "description": "password that is going to replace the old one",
            "required": true
          },
          {
            "in": "formData",
            "name": "password_confirmation",
            "type": "string",
            "description": "confirmation password",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "succesful operation"
          },
          "422": {
            "description": "Invalid input"
          }
        }
      }
    },
    "/password/forgot": {
      "post": {
        "tags": [
          "password"
        ],
        "summary": "Obtain token to recover password",
        "description": "It sends an email to the user with a recover password token",
        "operationId": "forgotPass",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "Email",
            "type": "string",
            "description": "Email of the password that need recovery",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "succesful operation"
          },
          "404": {
            "description": "User not found"
          },
          "422": {
            "description": "Token expired"
          }
        }
      }
    },
    "/password/recover": {
      "post": {
        "tags": [
          "password"
        ],
        "summary": "Submit new password",
        "description": "",
        "operationId": "recoverPass",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "token",
            "description": "recover token sendend to the user",
            "required": true,
            "type": "string"
          },
          {
            "in": "formData",
            "name": "password",
            "type": "string",
            "description": "password that is going to replace the old one",
            "required": true
          },
          {
            "in": "formData",
            "name": "password_confirmation",
            "type": "string",
            "description": "confirmation password",
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation"
          },
          "400": {
            "description": "Invalid status value"
          }
        }
      }
    },
    "/products": {
      "get": {
        "tags": [
          "product"
        ],
        "summary": "List of products",
        "description": "List the products available in the store with pagination, it accepts search by name, order by filters and search by minimun of tags",
        "operationId": "listOfProducts",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "tags",
            "in": "query",
            "description": "value of tags of each tag",
            "type": "array",
            "items": {
              "type": "integer"
            }
          },
          {
            "name": "scope",
            "in": "query",
            "description": "filter to order results",
            "type": "string",
            "enum": [
              "names",
              "likes",
              "dislikes"
            ]
          },
          {
            "name": "search",
            "in": "query",
            "description": "value to search by name",
            "type": "string"
          },
          {
            "name": "page",
            "in": "query",
            "description": "pagination page, six products per page",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "successful operation",
            "schema": {
              "$ref": "#/definitions/SearchResponse"
            }
          },
          "400": {
            "description": "Invalid tag value"
          }
        }
      }
    },
    "/like": {
      "post": {
        "tags": [
          "product"
        ],
        "summary": "Assigns a kind of like to a product",
        "description": "",
        "operationId": "assignLike",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "product",
            "in": "query",
            "description": "ID of the product to assign like",
            "required": true,
            "type": "integer"
          },
          {
            "name": "type",
            "in": "query",
            "description": "ID of the product to assign like",
            "required": true,
            "type": "string",
            "enum": [
              "likes",
              "dislikes"
            ]
          }
        ],
        "responses": {
          "204": {
            "description": "No content"
          }
        }
      }
    },
    "/order": {
      "get": {
        "tags": [
          "order"
        ],
        "summary": "Obtains all orders from a the logged user",
        "description": "",
        "operationId": "allOrders",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Returns all orders",
            "schema": {
              "$ref": "#/definitions/Orders"
            }
          }
        }
      }
    },
    "/order/{id}": {
      "put": {
        "tags": [
          "order"
        ],
        "summary": "updates current order to complete status",
        "description": "",
        "operationId": "checkoutOrder",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "description": "ID of the current order",
            "required": true,
            "type": "integer",
            "format": "int64"
          }
        ],
        "responses": {
          "204": {
            "description": "successful operation"
          }
        }
      },
      "delete": {
        "tags": [
          "order"
        ],
        "summary": "Soft deletes the current order with status canceled",
        "description": "",
        "operationId": "cancelOrder",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "type": "integer"
          }
        ],
        "responses": {
          "204": {
            "description": "successful operation"
          }
        }
      },
      "get": {
        "tags": [
          "order"
        ],
        "summary": "Obtains the details of an order",
        "description": "",
        "operationId": "showOrder",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "required": true,
            "description": "",
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "returns the details of an order",
            "schema": {
              "$ref": "#/definitions/Details"
            }
          },
          "400": {
            "description": "Invalid Order"
          }
        }
      }
    },
    "/order_ines": {
      "post": {
        "tags": [
          "order_line"
        ],
        "summary": "Add a product to an order",
        "operationId": "addProduct",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "orderline[product_id]",
            "description": "Id of the product",
            "required": true,
            "type": "integer"
          },
          {
            "in": "formData",
            "name": "orderline[quantity]",
            "description": "Quantity of the product",
            "required": true,
            "type": "integer"
          },
          {
            "in": "formData",
            "name": "orderline[price]",
            "description": "Price of the product",
            "required": true,
            "type": "number"
          }
        ],
        "responses": {
          "201": {
            "description": "successful creation"
          },
          "404": {
            "description": "Product not found"
          }
        }
      }
    },
    "/oauth/token": {
      "post": {
        "tags": [
          "login"
        ],
        "summary": "Obtain token",
        "description": "",
        "operationId": "login",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "email",
            "description": "Email of the user",
            "required": true,
            "type": "string"
          },
          {
            "in": "formData",
            "name": "password",
            "description": "password of the user",
            "required": true,
            "type": "string"
          },
          {
            "in": "formData",
            "name": "grant_type",
            "description": "Type of access",
            "required": true,
            "type": "string",
            "enum": [
              "password"
            ]
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "petstore_auth": {
      "type": "oauth2",
      "authorizationUrl": "http://petstore.swagger.io/oauth/dialog",
      "flow": "implicit",
      "scopes": {
        "write:pets": "modify pets in your account",
        "read:pets": "read your pets"
      }
    },
    "api_key": {
      "type": "apiKey",
      "name": "api_key",
      "in": "header"
    }
  },
  "definitions": {
    "Comments": {
      "type": "object",
      "properties": {
        "user": {
          "type": "string"
        },
        "body": {
          "type": "string"
        },
        "rate": {
          "type": "integer"
        }
      }
    },
    "Details": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "status": {
          "type": "string",
          "description": "order status",
          "enum": [
            "complete",
            "abandoned",
            "canceled",
            "open"
          ]
        },
        "products": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/OrderProducts"
          }
        },
        "comments": {
          "$ref": "#/definitions/Comments"
        }
      }
    },
    "Orders": {
      "type": "object",
      "properties": {
        "orders": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Orders"
          }
        }
      }
    },
    "Order": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "status": {
          "type": "string",
          "description": "order status",
          "enum": [
            "complete",
            "abandoned",
            "canceled",
            "open"
          ]
        },
        "products": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/OrderProducts"
          }
        }
      }
    },
    "OrderProducts": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "price": {
          "type": "number"
        },
        "quantity": {
          "type": "integer"
        }
      }
    },
    "SearchResponse": {
      "type": "object",
      "properties": {
        "products": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/ListOfProducts"
          }
        },
        "total_pages": {
          "type": "integer"
        }
      }
    },
    "ListOfProducts": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer"
        },
        "name": {
          "type": "string"
        },
        "image_url": {
          "type": "string"
        },
        "stock": {
          "type": "integer"
        },
        "price": {
          "type": "number"
        },
        "like_count": {
          "type": "integer"
        },
        "dislike_count": {
          "type": "integer"
        },
        "tags": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/Tag"
          }
        }
      }
    },
    "Tag": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "name": {
          "type": "string"
        }
      }
    }
  }
}