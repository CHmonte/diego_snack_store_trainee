module CommentModule
  def setup_next_comment(type, object)
    @type = type
    @commentable = object
    @comments = set_comments(object).joins(:user).where('users.active')
    @comment = @commentable.comments.new
  end

  private 
  def set_comments(object)
    if object.is_a?(Profile)
      if current_user.admin > 0
        object.comments
      else
        object.comments.where('publish OR user_id = ?', current_user.id)
      end
    else
      object.comments
    end
  end
end