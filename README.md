# README
DIEGO_SNACK_STORE

From the root the following commands:
  docker-compose up
  docker-compose run web rails db:seed

This will create the the databases and will seed it propperly

Configure a .env file with the following data:
  DEV_DATABASE= <- Name of the development database ->
  DEV_USERNAME= <- Valid username in postgresql DB ->
  DEV_PASSWORD= <- Password of username in postgresql DB ->
  DEV_HOST=     <- Host direction of database ->

  JWT_SECRET=   <- SECRET KEY to encode JWT token ->

  <-Configure the following amazon's data->
  AMAZON_ACCESS_KEY_ID=
  AMAZON_SECRET=
  AMAZON_REGION=
  AMAZON_BUCKET=

  REDIS_URL= <- REDIS URL ->

  STRIPE_DEV_PUBLISHABLE_KEY= <- STRIPE public test key ->
  STRIPE_DEV_SECRET_KEY= <- STRIPE secret test key ->

  UID=1000

The database will be populated with the two following users:
  ADMIN: admin@example.com
      PASS: admin01
  
  SUPPORT USER: example2@example.com
      PASS: 1234567 

  NORMAL USER: example@example.com
      PASS: 1234567
